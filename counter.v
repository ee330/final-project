module counter(incr, state);
	parameter BIT_WIDTH;

	input                     incr;
	output  [(BIT_WIDTH-1):0] state;
endmodule
