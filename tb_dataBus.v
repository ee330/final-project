module tb_dataBus();
   parameter DBUS_WIDTH       = 4;
   parameter CMD_WIDTH        = 8;

   reg                        clk, sel;
   reg     [(DBUS_WIDTH-1):0] dBus;
   reg                        reset;
   wire                       sync;
   wire    [(CMD_WIDTH-1) :0] instr;
   dataBus #(DBUS_WIDTH, CMD_WIDTH)
      db(.clk(clk), .reset(reset), .sel(sel), .dBus(dBus), .sync(sync), .dByte(instr));
   always begin
      #5 clk                  = ~clk;
   end
   initial begin
      clk                     = 1'b0;
      reset                   = 1'b1;
      dBus                    = 4'b0000;
   end
   always #10 dBus[0]         = ~dBus[0];
   always #20 dBus[1]         = ~dBus[1];
   always #40 dBus[2]         = ~dBus[2];
   always #80 dBus[3]         = ~dBus[3];
   always begin
      #5  reset               = 1'b0;
      #5  sel                 = 1'b1;
      #20 sel                 = 1'b0;
      #10 sel                 = 1'b1;
      #10 sel                 = 1'b0;
      #20 sel                 = 1'b1;
      #40 sel                 = 1'b0;
      #5  reset               = 1'b1;
   end
endmodule
