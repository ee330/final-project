module tb_regFile();
   parameter S_ADDR_WIDTH;
   parameter S_COUNT;
   parameter ASCI_WIDTH;

   reg                           clk, reset;     
   reg      [(S_ADDR_WIDTH-1):0] ra, rb;
   reg      [(ASCI_WIDTH-1)  :0] wData;
   reg                           RE, WE;

   wire     [(ASCI_WIDTH-1)  :0] rData;

   regFile #(S_ADDR_WIDTH, S_COUNT, ASCI_WIDTH)
      rFile(.clk(clk), .reset(reset), .ra(ra), .rb(rb), .rData(rData), .wData(wData), .RE(RE), .WE(WE));

   integer i;
   reg [(ASCI_WIDTH-1):0] data [(S_COUNT-1):0];
   always begin
      #5 clk = ~clk;
   end
   always @(posedge clk) begin
      i = i + 1;
      if(i<S_COUNT) begin
         wData = i;
         i = i + 1;
      end else
         wData = 0;
         i = 0;
   end
   always begin
      #70 reset = 1;
      #10 reset = 0;
      #70 WE = 1;
      #70 WE = 0;
      #140 WE = 1;
      #70 WE = 0;
   end
endmodule
