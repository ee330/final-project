module regFile(clk, ra, rb, rData, wData, WE); //, RE);
   parameter S_ADDR_WIDTH;
   parameter S_COUNT;
   parameter ASCI_WIDTH;

   input                           clk;     
   input      [(S_ADDR_WIDTH-1):0] ra, rb;
   input      [(ASCI_WIDTH-1)  :0] wData;
   input                           WE; //, RE

   output     [(ASCI_WIDTH-1)  :0] rData;

   integer i;
   reg [(ASCI_WIDTH-1):0] data [(S_COUNT-1):0];
	//rData is latched, only update when RE
   always @(posedge clk) begin
      //if(RE) begin          //For RE on clk
      //   rData = data[ra];
      //end else begin
      //   rData = rData;
      //end
      if(WE) begin
         data[rb] = wData;
      end
   end
   assign rData = data[ra];      //For Always Read
endmodule
