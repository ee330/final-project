module dataBus(clk, reset, sel, dBus, sync, dByte);
   parameter DBUS_WIDTH;
   parameter CMD_WIDTH;

   input                         clk, sel, reset;
   input   [(DBUS_WIDTH-1):0]    dBus;
   output reg                    sync;
   output reg [(CMD_WIDTH-1) :0] dByte;
   reg   [(DBUS_WIDTH-1):0]      low;
   reg                           state;

   always @(posedge clk, posedge reset) begin
      if(reset) begin
         state      = 1'b0;
         low        = {DBUS_WIDTH{1'b0}};
         dByte      = {CMD_WIDTH{1'b0}};
         sync       = 1'b0;
      end else if(sel==1) begin
         if(state==0) begin
            state   = 1'b1;
            low     = dBus;
            dByte   = dByte;
            sync    = 1'b0;
         end else begin
            state   = 1'b0;
            low     = low;
            dByte   = {dBus,low};
            sync    = 1'b1;
         end
      end else begin
         state      = 1'b0;
         low        = low;
         dByte      = dByte;
         sync       = 1'b0;
      end
   end
endmodule
