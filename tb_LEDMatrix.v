module tb_LEDMatrix();
   parameter     DBUS_WIDTH             = 4;
   parameter     CHAR_HEIGHT            = 7;
   parameter     CHAR_WIDTH             = 5;
   parameter     CMD_WIDTH              = 8;

   parameter     S_ADDR_WIDTH           = 5;
   parameter     S_COUNT                = 1 << S_ADDR_WIDTH;
   parameter     INSTR_DATA_WIDTH       = 7;
   parameter     ASCI_WIDTH             = INSTR_DATA_WIDTH;
   parameter     ROW_ID_WIDTH           = 3;

   parameter     DATA_CLK               = 22;
   parameter     DATA_CLK_H             = 11;


   reg                                  eClk, iClk, reset, sel;
   reg         [(DBUS_WIDTH-1)  :0]     dBus;
   wire        [(CHAR_HEIGHT-1) :0]     rowDriver;
   wire        [(CHAR_WIDTH-1)  :0]     portA, portB, portC;

   reg         [(CHAR_WIDTH-1)  :0]     charA [(CHAR_HEIGHT-1):0];
   reg         [(CHAR_WIDTH-1)  :0]     charB [(CHAR_HEIGHT-1):0];
   reg         [(CHAR_WIDTH-1)  :0]     charC [(CHAR_HEIGHT-1):0];

   reg         [2               :0]     row;
   reg         [(CMD_WIDTH-1)   :0]     cmds [15:0];

   LEDMatrix mat(.eClk(eClk), .iClk(iClk), .reset(reset), .sel(sel), .dBus(dBus), .rowDriver(rowDriver), .portA(portA), .portB(portB), .portC(portC));

   always @(rowDriver) begin
      case(rowDriver)
         7'd1 :  row = 0;
         7'd2 :  row = 1;
         7'd4 :  row = 2;
         7'd8 :  row = 3;
         7'd16:  row = 4;
         7'd32:  row = 5;
         7'd64:  row = 6;
         default row = 7;
      endcase
   end
   always @(row) begin
      charA[row] = portA;
      charB[row] = portB;
      charC[row] = portC;
   end
   initial begin
      iClk  = 0;
      eClk  = 0;
      reset = 1;
      sel   = 0;
      dBus  = 0;
      cmds[0] = {1'b0, 7'd11}; //Set Speed to 1
      cmds[1] = {1'b0, 7'd1 };
      cmds[2] = {1'b1, 7'd65};
      cmds[3] = {1'b1, 7'd66};
      cmds[4] = {1'b1, 7'd67};
      cmds[5] = {1'b1, 7'd68};
   end
   always #2  iClk = ~iClk;
   always #DATA_CLK_H eClk = ~eClk;
   always begin
      #DATA_CLK_H reset = 0;
      #DATA_CLK_H begin
                  sel   = 1;
                  dBus  = cmds[0][3:0];
      end
      #DATA_CLK   dBus  = cmds[0][7:4];
      #DATA_CLK   dBus  = cmds[1][3:0];
      #DATA_CLK   dBus  = cmds[1][7:4];
      #DATA_CLK   dBus  = cmds[2][3:0];
      #DATA_CLK   dBus  = cmds[2][7:4];
      #DATA_CLK   dBus  = cmds[3][3:0];
      #DATA_CLK   dBus  = cmds[3][7:4];
      #DATA_CLK   dBus  = cmds[4][3:0];
      #DATA_CLK   dBus  = cmds[4][7:4];
      #DATA_CLK   dBus  = cmds[5][3:0];
      #DATA_CLK   dBus  = cmds[5][7:4];
   end
endmodule
