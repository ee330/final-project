onerror {resume}
vsim work.tb_dataBus
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Control -height 30 -label reset /tb_dataBus/reset
add wave -noupdate -expand -group Control -height 30 -label clk /tb_dataBus/clk
add wave -noupdate -expand -group Control -height 30 -label select /tb_dataBus/sel
add wave -noupdate -expand -group Control -height 30 -label {hByte Input} /tb_dataBus/dBus
add wave -noupdate -expand -group {Internal Register} -height 30 -label {1st hByte} /tb_dataBus/db/low
add wave -noupdate -expand -group {Internal Register} -height 30 -label {1st hByte Received} /tb_dataBus/db/state
add wave -noupdate -expand -group Output -height 30 -label {Byte Ready} /tb_dataBus/sync
add wave -noupdate -expand -group Output -height 30 -label Byte /tb_dataBus/instr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Byte 1 begin} {15 ps} 1} {{Byte 1 end} {35 ps} 1} {{incomplete byte} {55 ps} 1} {{Byte 2 & 3 begin} {75 ps} 1} {{Byte 2 & 3 end} {115 ps} 1}
quietly wave cursor active 5
configure wave -namecolwidth 170
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {130 ps}
run 125 ps
