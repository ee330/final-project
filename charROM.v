module charROM (address, offset, rData);
   parameter ADDR_WIDTH;
   parameter OFFSET_WIDTH;
   parameter NUM_BLOCKS;
   parameter BLOCK_WIDTH;
   parameter BITS = NUM_BLOCKS * BLOCK_WIDTH;

   input   [(ADDR_WIDTH-1)  :0] address;
   input   [(OFFSET_WIDTH-1):0] offset;
   reg     [(BITS-1) :0]        char_out;
   reg     [(BITS-1) :0]        tmp;
   output  [(BLOCK_WIDTH-1) :0] rData;

   always @(address, offset) begin
      case(address)
         7'h20:   char_out = 35'b00000000000000000000000000000000000; // Blank
         7'h30:   char_out = 35'b11111100011001110101110011000111111; // '0'
         7'h31:   char_out = 35'b11100001000010000100001000010011111; // '1'
         7'h32:   char_out = 35'b11111000010000111111100001000011111; // '2'
         7'h33:   char_out = 35'b11111000010000100111000010000111111; // '3'
         7'h34:   char_out = 35'b10001100011111100001000010000100001; // '4'
         7'h35:   char_out = 35'b11111100001000001111000010000111110; // '5'
         7'h36:   char_out = 35'b11111100001000011111100011000111111; // '6'
         7'h37:   char_out = 35'b11111000010000100001000010000100001; // '7'
         7'h38:   char_out = 35'b01110100011000101110100011000101110; // '8'
         7'h39:   char_out = 35'b11111100011000111111000010000100001; // '9'
    
         7'd65:   char_out = 35'b01110100011000111111100011000110001; // 'A'
         7'd66:   char_out = 35'b11110100011000111110100011000111110; // 'B'
         7'd67:   char_out = 35'b11111100001000010000100001000011111; // 'C'
         7'd68:   char_out = 35'b11110100011000110001100011000111110; // 'D'
         7'h69:   char_out = 35'b11111100001000011100100001000011111; // 'E'
         7'h70:   char_out = 35'b11111100001000011100100001000010000; // 'F'
         7'h71:   char_out = 35'b11111100011000010000101111000111111; // 'G'
         7'h72:   char_out = 35'b10001100011000111111100011000110001; // 'H'
         7'h73:   char_out = 35'b11111001000010000100001000010011111; // 'I'
         7'h74:   char_out = 35'b11111000100001000010000101001001110; // 'J'
         7'h75:   char_out = 35'b10010100101010011000101001001010010; // 'K'
         7'h76:   char_out = 35'b10000100001000010000100001000011111; // 'L'
         7'h77:   char_out = 35'b10001110111101110101100011000110001; // 'M'
         7'h78:   char_out = 35'b10001110011010110101100111000110001; // 'N'
         7'h79:   char_out = 35'b11111100011000110001100011000111111; // 'O'
         7'h80:   char_out = 35'b11111100011000111111100001000010000; // 'P'
         7'h81:   char_out = 35'b11111100011000110001101011001011101; // 'Q'
         7'h82:   char_out = 35'b11111100011000111110101001001010001; // 'R'
         7'h83:   char_out = 35'b11111100001000011111000010000111111; // 'S'
         7'h84:   char_out = 35'b11111001000010000100001000010000100; // 'T'
         7'h85:   char_out = 35'b10001100011000110001100011000111111; // 'U'
         7'h86:   char_out = 35'b10001100011000110001010100101000100; // 'V'
         7'h87:   char_out = 35'b10001100011000110101101010101001010; // 'W'
         7'h88:   char_out = 35'b10001100010101000100010101000110001; // 'X'
         7'h89:   char_out = 35'b10001100011000101010001000010000100; // 'Y'
         7'h90:   char_out = 35'b11111000010001000100010001000011111; // 'Z'
         default: char_out = 35'b00000000000000000000000000000000000; // Blank
      endcase
      tmp = char_out >> (BLOCK_WIDTH * offset);
   end
   assign rData = tmp[(BLOCK_WIDTH-1):0];
endmodule
