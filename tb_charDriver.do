onerror {resume}
vsim work.tb_charDriver
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Reference -height 30 -label A /tb_charDriver/A
add wave -noupdate -expand -group Reference -height 30 -label B /tb_charDriver/B
add wave -noupdate -expand -group {TB State} -height 30 -label counter /tb_charDriver/id
add wave -noupdate -expand -group {TB State} -height 30 -label state /tb_charDriver/state
add wave -noupdate -expand -group Control -height 30 -label clk /tb_charDriver/clk
add wave -noupdate -expand -group Control -height 30 -label reset /tb_charDriver/reset
add wave -noupdate -expand -group Control -height 30 -label WE /tb_charDriver/WE
add wave -noupdate -expand -group Control -height 30 -label Data /tb_charDriver/wData
add wave -noupdate -expand -group Output -height 30 -label {Char A Output} /tb_charDriver/aData
add wave -noupdate -expand -group Output -height 30 -label {Char B Output} /tb_charDriver/bData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {Reset {5 ps} 1} {{Write A} {15 ps} 1} {{Write B} {85 ps} 1} {{Cycle 1} {155 ps} 1} {{Cycle 2} {225 ps} 1} {End {295 ps} 1}
quietly wave cursor active 6
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {320 ps}
run 305 ps