module tb_encoder();
   parameter INPUT_WIDTH  = 3;
   parameter OUTPUT_WIDTH = 7;

   reg   [(INPUT_WIDTH-1) :0] dIn;
   reg                        clk;
   wire  [(OUTPUT_WIDTH-1):0] dOut;

   encoder #(INPUT_WIDTH, OUTPUT_WIDTH)
      enc(.clk(clk), .dIn(dIn), .dOut(dOut));
   initial begin
      dIn = 0;
      clk = 0;
   end
   always #5 clk = ~clk;
   always begin
      #10 dIn = dIn + 1;
   end
endmodule
