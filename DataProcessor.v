module dataProcessor(clk, reset, sync, cmd, data, rfWA, rfWD, rfWE, sLen, rIdx, setIdx);
   parameter  S_ADDR_WIDTH = 5;
   parameter  INSTR_DATA_WIDTH = 7;
   parameter  C_CLR                    = 0;
   parameter  C_LF                     = 10;
   parameter  C_RI                     = 2;
   parameter  C_EOS                    = 3;
   parameter  C_WI                     = 13;
   parameter  C_SP					   = 11;

   parameter  S_CLR                    = 0; //Clear
   parameter  S_CHR                    = 1; //Write Character
   parameter  S_LF                     = 2; //Line Feed
   parameter  S_RI1                    = 3; //Set Read Index CMD
   parameter  S_RI2                    = 4; //Set Read Index Data
   parameter  S_WI1                    = 5; //Set Write Index CMD
   parameter  S_WI2                    = 6; //Set Write Index Data
   parameter  S_EOS                    = 7; //End of String
   parameter  S_SP1                    = 8; //Set Speed CMD
   parameter  S_SP2                    = 9; //Set Speed Data

   input                               clk, reset, cmd;
   input                               sync;
   input      [(INSTR_DATA_WIDTH-1):0] data;
   reg        [(S_ADDR_WIDTH-1)    :0] wIndex;
   output reg [(S_ADDR_WIDTH-1)    :0] rfWA, sLen, rIdx;
   output reg [(INSTR_DATA_WIDTH-1):0] rfWD, speed;
   output reg                          rfWE, setIdx, setSpeed;

   reg [3:0]                           state;

   //update state on sync edge
   always @(posedge sync, posedge reset) begin
      if(reset) begin
         state                         = S_CLR;
      end else begin
         if(cmd) begin
            state                      = S_CHR;
         end else begin
            case(state)
               S_RI1: state            = S_RI2; //Set Character Read  Index (3-2)
               S_WI1: state            = S_WI2; //Set Character Write Index (5-2)
		       S_SP1: state			   = S_SP2;
               default: begin               //1st byte of CMD
                  case(data[3:0])
                     C_CLR: state      = S_CLR; //Clear Data
                     C_LF:  state      = S_LF;  //LF (Write Index to 0)
                     C_RI:  state      = S_RI1; //Set Character Read  Index (3-1)
                     C_WI:  state      = S_WI1; //Set Character Write Index (5-1)
                     C_EOS: state      = S_EOS;
					 C_SP:  state	   = S_SP1;
                     default: state    = 15; //DO NOTHING
                  endcase
               end
            endcase
         end
      end
   end
   //Timing Latches
   reg spike;
   reg toggle;
   reg old;
   //Timing Triggers
   wire nSync;
   assign nSync = ~sync & old;
   wire pSync;
   assign pSync = sync & ~old;
   wire pToggle;
   assign pToggle = clk & ~spike & ~toggle;
   wire nToggle;
   assign nToggle = ~clk & spike & toggle;
   //Timing
   wire nSyncReset;
   assign nSyncReset = reset | nSync;
   always @(posedge pSync, posedge nSyncReset) begin
      if(nSyncReset) begin
         old = 0;
      end else if(pSync) begin
         old = 1;
      end else begin
         old = old;
      end
   end
   wire pToggleReset;
   assign pToggleReset = pToggle | reset;
   always @(posedge pSync, posedge pToggleReset) begin
      if(pToggleReset) begin
         spike = 1;
      end else if(pSync) begin
         spike = 0;
      end else begin
         spike = spike;
      end
   end
   wire nToggleReset;
   assign nToggleReset = nToggle | reset;	
   always @(posedge pToggle, posedge nToggleReset) begin
      if(nToggleReset) begin
         toggle = 0;
      end else if(pToggle) begin
         toggle = 1;
      end else begin
         toggle = toggle;
      end
   end

   //update Write index on State Chage
   always @(negedge spike, posedge reset) begin
      if(reset) begin
         wIndex = 0;
      end else begin
         case(state)
            S_CLR: wIndex                 = 0;
            S_CHR: wIndex                 = (wIndex + 1);
            S_LF:  wIndex                 = 0;
            S_WI2: wIndex                 = data;
            default: wIndex               = wIndex;
         endcase
      end
   end
   wire spikeNToggleReset;
   assign spikeNToggleReset = (spike & ~toggle) | reset;
   always @(negedge spike, posedge spikeNToggleReset) begin
      if(spikeNToggleReset) begin
		    setSpeed 				   = 0;
            setIdx                     = 0;
            rfWE                       = 0;
      end else begin
         case(state)
            S_CLR: begin                           //Clear Data
               setSpeed                   = 0;
               setIdx                     = 1;
               rfWE                       = 0;
            end
            S_CHR: begin                           //Write Next Character
               setSpeed                   = 0;
               setIdx                     = 0;
               rfWE                       = 1;
            end
            S_RI2: begin                           //Set Character Read Index
               setSpeed                   = 0;
               setIdx                     = 1;
               rfWE                       = 0;
            end
            S_SP2: begin
               setSpeed                   = 1;
               setIdx                     = 0;
               rfWE                       = 0;
            end
            default: begin
               setSpeed                   = 0;
               setIdx                     = 0;
               rfWE                       = 0;
            end
         endcase
      end
   end
   always @(negedge spike, posedge reset) begin
      if(reset) begin
         speed = 63;   //~1 sec
      end else if(state==S_SP2) begin
         speed = data;
      end else begin
         speed = speed;
      end
   end
   always @(negedge spike, posedge reset) begin
      if(reset) begin
         rIdx                       = 0;
         rfWA                       = 0;
         rfWD                       = 0;
      end else begin
         case(state)
            S_CHR: begin                           //Write Next Character
               rIdx                       = 0;
               rfWA                       = wIndex-1;
               rfWD                       = data;
            end
            S_RI2: begin                           //Set Character Read Index
               rIdx                       = data;
               rfWA                       = 0;
               rfWD                       = 0;
            end
            default: begin
               rIdx                       = 0;
               rfWA                       = 0;
               rfWD                       = 0;
            end
         endcase
      end
   end
   always @(negedge spike, posedge reset) begin
      if(reset) begin
         sLen                          <= 0;
      end else if(state == S_CLR) begin
         sLen                          <= 0;
      end else if(sLen<wIndex || state == S_EOS) begin
         sLen                          <= wIndex;
      end else begin
         sLen                          <= sLen;
      end
   end
endmodule