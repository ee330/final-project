module charProcessor(clk, reset, setIdx, idx, setSpeed, speed, sLen, rfRA, ROMoffset, charWE);
   parameter S_ADDR_WIDTH;
   parameter OFFSET_WIDTH;
   parameter INSTR_DATA_WIDTH;

   input                               clk, reset, setIdx, setSpeed;
   input      [(S_ADDR_WIDTH-1):    0] sLen, idx;

   input      [(INSTR_DATA_WIDTH-1):0] speed;
   reg        [(INSTR_DATA_WIDTH+3):0] count;

   output reg [(S_ADDR_WIDTH-1):    0] rfRA;
   output reg [(OFFSET_WIDTH-1):    0] ROMoffset;
   reg        [(OFFSET_WIDTH-1):    0] state;
   output                              charWE;

   reg                                 set, complete;
   reg        [(INSTR_DATA_WIDTH+3):0] delay;

   always @(posedge setSpeed, posedge reset) begin
      if(reset) begin
         delay = ((7*64)-1);
      end else begin
         delay = 7*(speed+1)-1;
      end
   end
   wire sReset;
   assign sReset = reset | setIdx | setSpeed;
   always @(posedge clk, posedge sReset) begin
      if(sReset) begin
         count                    <= -1;
         state                     = 7;
      end else begin
         if(count >= delay) begin
            count                 <= 0;
            state                  = 0;
         end else begin
            count                 <= count + 1;
            if(state < 7) begin
               state              <= state + 1;
            end
         end
      end
   end
   assign charWE = (state < 7);
   //set ROM Offset / Row Select
   always @(posedge clk, posedge sReset) begin
      if(sReset) begin
         ROMoffset                <= 7;
      end else begin
         if(ROMoffset < 6) begin
            ROMoffset             <= ROMoffset + 1;
         end else begin
            ROMoffset             <= 0;
         end
      end
   end
   //set RF read address
   always @(posedge clk, posedge sReset) begin
      if(sReset) begin
         if(reset) begin
            rfRA                         <= -1;
         end else if(setIdx) begin
            rfRA                         <= idx -1;
         end else if(setSpeed) begin
            rfRA                         <= rfRA -1;
         end
      end else begin
         if(count >= delay) begin
            if(rfRA < (sLen-1)) begin
               rfRA                   <= rfRA + 1;
            end else begin
               rfRA                   <= 0;
            end
         end else begin
            rfRA                      <= rfRA;
         end
      end
   end
endmodule
