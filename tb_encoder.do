onerror {resume}
vsim work.tb_encoder
quietly WaveActivateNextPane {} 0
add wave -noupdate -height 30 -label clk /tb_encoder/clk
add wave -noupdate -height 30 -label input -radix unsigned /tb_encoder/dIn
add wave -noupdate -height 30 -label output /tb_encoder/dOut
TreeUpdate [SetDefaultTree]
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {100 ps}
run 95 ps
