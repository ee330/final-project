module tb_charDriver();
   parameter CHAR_WIDTH                   = 5;
   parameter CHAR_HEIGHT                  = 7;
   
   reg                                    clk, reset, WE;
   reg     [(CHAR_WIDTH-1):0]             wData;
   wire    [(CHAR_WIDTH-1):0]             aData;
   wire    [(CHAR_WIDTH-1):0]             bData;
   reg     [(CHAR_WIDTH*CHAR_HEIGHT)-1:0] A, B;
   integer                                id;
   integer                                state;

   charDriver #(CHAR_WIDTH, CHAR_HEIGHT)
      aDrive(.clk(clk),.reset(reset),.WE(WE),.wData(wData),.rData(aData)),
      bDrive(.clk(clk),.reset(reset),.WE(WE),.wData(aData),.rData(bData));
   initial begin
      A                                   = 35'b01110_10001_10001_11111_10001_10001_10001;
      B                                   = 35'b11110_10001_10001_11110_10001_10001_11110;
      id                                  = 0;
      reset                               = 1;
      clk                                 = 0;
      WE                                  = 0;
      wData                               = 0;
   end
   always begin
      #5 clk                              = ~clk;
   end
   always @(posedge clk) begin
      if(reset == 1) begin
         state                            = 0;
      end else if(id < CHAR_HEIGHT) begin
         state                            = 1;
      end else if (id<(2*CHAR_HEIGHT)) begin
         state                            = 2;
      end else begin
         state                            = 3;
      end
   end
   always @(state, posedge clk) begin
      case(state)
         0: begin
            wData                         = 0;
            id                           <= 0;
         end
         1: begin
            wData                         = A >> CHAR_WIDTH*id;
            id                           <= id + 1;
         end
         2: begin
            wData                         = B >> CHAR_WIDTH*(id-CHAR_HEIGHT);
            id                           <= id + 1;
         end
         3: begin
            wData                         = A;
            id                           <= 0;
         end
      endcase
   end
   always begin
      #10  reset                          = 0;
      #10  WE                             = 1;
      #140 WE                             = 0;
      #140 reset                          = 1;
   end
endmodule
