module dataBus(clk, reset, sel, dataIn, ready, dataOut);
        parameter DBUS_WIDTH;
        parameter CMD_WIDTH;

	input                         clk, sel, reset;
	input   [(DBUS_WIDTH-1):0]    dataIn;
	output                        ready;
	output reg [(CMD_WIDTH-1) :0] dataOut;
   reg   [(DBUS_WIDTH-1):0] low;
   reg state;

   always @(posedge clk, posedge reset) begin
      if(reset) begin
         low = {DBUS_WIDTH{1'b0}};
         dataOut = {CMD_WIDTH{1'b0}};
      end else if(sel==1) begin
         if(state==0) begin
            low = dataIn;
            state = 1'b1;
         end else begin
            state = 1'b0;
            dataOut = {dataIn,low};
         end
      end else begin
         state = 1'b0;
      end
   end
   assign ready = ~state & sel & ~reset;
endmodule