module charDriver(clk, reset, WE, wData, rData);
   parameter CHAR_WIDTH;
   parameter CHAR_HEIGHT;

   input                                  clk, reset, WE;
   input   [(CHAR_WIDTH-1):0]             wData;
   output  [(CHAR_WIDTH-1):0]             rData;

   reg     [(CHAR_WIDTH*CHAR_HEIGHT-1):0] matrix;

   always @(posedge clk, posedge reset) begin
      if(reset) begin
         matrix                           = {(CHAR_WIDTH*CHAR_HEIGHT){1'b0}};
      end else if(WE) begin
         matrix                           = {wData,matrix[((CHAR_WIDTH*CHAR_HEIGHT)-1):CHAR_WIDTH]};
      end else begin
         matrix                           = {matrix[(CHAR_WIDTH-1):0],matrix[((CHAR_WIDTH*CHAR_HEIGHT)-1):CHAR_WIDTH]};
      end
   end
   assign rData                           = matrix[(CHAR_WIDTH-1):0];
endmodule
