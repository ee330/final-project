module tb_dataProcessor();
   parameter  S_ADDR_WIDTH             = 5;
   parameter  S_COUNT                  = 1 << S_ADDR_WIDTH;
   parameter  INSTR_DATA_WIDTH         = 7;
   parameter  ASCI_WIDTH               = INSTR_DATA_WIDTH;

   reg                                 clk, reset, cmd;
   reg                                 sync;
   reg        [(INSTR_DATA_WIDTH-1):0] data;
   wire       [(S_ADDR_WIDTH-1)    :0] rfWA, sLen, rIdx;
   wire       [(INSTR_DATA_WIDTH-1):0] rfWD, speed;
   wire                                rfWE, setIdx;

   dataProcessor #(S_ADDR_WIDTH, INSTR_DATA_WIDTH)
             dataProc(.clk(clk), .reset(reset), .rIdx(rIdx), .setIdx(setIdx), .sync(sync), .cmd(cmd), .data(data), .rfWA(rfWA), .rfWD(rfWD), .rfWE(rfWE), .sLen(sLen), .speed(speed));
   wire       [(ASCI_WIDTH -1)     :0] rData;
   regFile       #(S_ADDR_WIDTH, S_COUNT, ASCI_WIDTH)
             rFile(.clk(clk), .ra(rfWA), .rData(rData), .rb(rfWA), .wData(rfWD), .WE(rfWE));
   initial begin
      clk   = 1;
      reset = 1;
   end
   always #1 clk  = ~clk;
   always begin
      #5   reset  = 0;
      #10 begin
           cmd    = 1;
           data   = 72; //H
           sync   = 1;
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 69; //E
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 76; //L
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 76; //L
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 79; //O
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 0;
           sync   = 1;
           data   = 10;  //Line Feed
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 1;
           sync   = 1;
           data   = 77; //M
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 0;
           sync   = 1;
           data   = 13; //Set Data Write Index
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 2; //Index 2
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 1;
           sync   = 1;
           data   = 79; //O
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 1;
           sync   = 1;
           data   = 87; //W
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 0;
           sync   = 1;
           data   = 3; //End Of String
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 2; //Set Read Index
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 1; //Index 1
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 0; //Clear
      end
      #5   sync   = 0;
      #5 begin
           cmd    = 0;
           sync   = 1;
           data   = 11; //Set Character Read Speed
      end
      #5   sync   = 0;
      #5 begin
           sync   = 1;
           data   = 31; //Speed 31
      end
      #5   sync   = 0;
   end
endmodule
