onerror {resume}
vsim work.tb_charProcessor
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Control -height 30 -label clk /tb_charProcessor/clk
add wave -noupdate -expand -group Control -height 30 -label reset /tb_charProcessor/reset
add wave -noupdate -expand -group Control -height 30 -label {string length} /tb_charProcessor/sLen
add wave -noupdate -expand -group Control -height 30 -label {set index} /tb_charProcessor/setIdx
add wave -noupdate -expand -group Control -height 30 -label index /tb_charProcessor/idx
add wave -noupdate -expand -group Control -height 30 -label {set speed} /tb_charProcessor/setSpeed
add wave -noupdate -expand -group Control -height 30 -label speed -radix decimal /tb_charProcessor/speed
add wave -noupdate -expand -group {Internal State} -height 30 -label {delay reset} -radix decimal /tb_charProcessor/cProc/delay
add wave -noupdate -expand -group {Internal State} -height 30 -label {delay counter} -radix unsigned /tb_charProcessor/cProc/count
add wave -noupdate -expand -group {Internal State} -height 30 -label state -radix unsigned /tb_charProcessor/cProc/state
add wave -noupdate -expand -group Output -height 30 -label {Read Address} /tb_charProcessor/rfRA
add wave -noupdate -expand -group Output -height 30 -label Offset -radix unsigned /tb_charProcessor/ROMoffset
add wave -noupdate -expand -group Output -height 30 -label {Write Character} /tb_charProcessor/charWE
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Write Character 0} {10 ps} 1} {{Write Character 1} {150 ps} 1} {{Loop - Write Character 0} {220 ps} 1} {{Set Character Index} {255 ps} 1} {{Write Character 7} {270 ps} 1} {{Loop - Write Character 0} {340 ps} 1} {Reset {410 ps} 1} {{Adjust Character Write Speed} {145 ps} 1}
quietly wave cursor active 8
configure wave -namecolwidth 236
configure wave -valuecolwidth 48
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
run 500 ps
WaveRestoreZoom {0 ps} {315 ps}
