module tb_charProcessor();
   parameter S_ADDR_WIDTH         = 5;
   parameter ROW_ID_WIDTH         = 3;
   parameter INSTR_DATA_WIDTH     = 7;


   reg                            clk, reset, setIdx, setSpeed;
   reg  [(S_ADDR_WIDTH-1)    :0]  sLen, idx;
   reg  [(INSTR_DATA_WIDTH-1):0]  speed;

   wire [(S_ADDR_WIDTH-1)    :0]  rfRA;
   wire [(ROW_ID_WIDTH-1)    :0]  ROMoffset;
   wire                           charWE;
   charProcessor #(S_ADDR_WIDTH, ROW_ID_WIDTH, INSTR_DATA_WIDTH)
             cProc(.clk(clk), .reset(reset), .setIdx(setIdx), .idx(idx), .setSpeed(setSpeed), .speed(speed), .sLen(sLen), .rfRA(rfRA), .ROMoffset(ROMoffset), .charWE(charWE));
   initial begin
      clk   = 1;
      reset = 1;
      speed = 1;
      sLen  = 2;
   end
   always #5 clk  = ~clk;
   always begin
      #3    reset  = 0;
      #170 begin
            setSpeed = 1;
            speed  = 0;
      end
      #5    setSpeed = 0;
      #105 begin
            setSpeed = 1;
            speed    = 0;
      end
      #5    setSpeed = 0;
      #105 begin
            sLen   = 8;
            idx    = 7;
            setIdx = 1;
      end
      #5    setIdx = 0;
      #40   sLen   = 7;
      #107  reset = 1;
   end
endmodule
