onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Control -height 30 -label {Data Clock} /tb_LEDMatrix/eClk
add wave -noupdate -expand -group Control -height 30 -label {Internal Clock} /tb_LEDMatrix/iClk
add wave -noupdate -expand -group Control -height 30 -label Reset /tb_LEDMatrix/reset
add wave -noupdate -expand -group Control -height 30 -label Select /tb_LEDMatrix/sel
add wave -noupdate -expand -group Control -height 30 -label {Half Byte} -radix hexadecimal /tb_LEDMatrix/dBus
add wave -noupdate -expand -group {rf Data} -label {rf Data} -radix ascii -childformat {{{/tb_LEDMatrix/mat/rFile/data[31]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[30]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[29]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[28]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[27]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[26]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[25]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[24]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[23]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[22]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[21]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[20]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[19]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[18]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[17]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[16]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[15]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[14]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[13]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[12]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[11]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[10]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[9]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[8]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[7]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[6]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[5]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[4]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[3]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[2]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[1]} -radix ascii} {{/tb_LEDMatrix/mat/rFile/data[0]} -radix ascii}} -radixshowbase 0 -subitemconfig {{/tb_LEDMatrix/mat/rFile/data[31]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[30]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[29]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[28]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[27]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[26]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[25]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[24]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[23]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[22]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[21]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[20]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[19]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[18]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[17]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[16]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[15]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[14]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[13]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[12]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[11]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[10]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[9]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[8]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[7]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[6]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[5]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[4]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[3]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[2]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[1]} {-height 15 -radix ascii} {/tb_LEDMatrix/mat/rFile/data[0]} {-height 15 -radix ascii}} /tb_LEDMatrix/mat/rFile/data
add wave -noupdate -expand -group {rf Write} -label Sync /tb_LEDMatrix/mat/db/sync
add wave -noupdate -expand -group {rf Write} -label Byte -radix hexadecimal /tb_LEDMatrix/mat/db/dByte
add wave -noupdate -expand -group {rf Write} -label {rf Write Enable} /tb_LEDMatrix/mat/dataProc/rfWE
add wave -noupdate -expand -group {rf Write} -label {rf Write Data} -radix ascii /tb_LEDMatrix/mat/dataProc/rfWD
add wave -noupdate -expand -group {rf Write} -label {rf Write Index} -radix unsigned /tb_LEDMatrix/mat/dataProc/rfWA
add wave -noupdate -expand -group {rf Read} -label {rf Read Index} -radix unsigned -radixshowbase 0 /tb_LEDMatrix/mat/charProc/rfRA
add wave -noupdate -expand -group {rf Read} -label {Char Lookup} -radix ascii -radixshowbase 0 /tb_LEDMatrix/mat/cROM/address
add wave -noupdate -expand -group {rf Read} -label {Char Row} -radix unsigned -radixshowbase 0 /tb_LEDMatrix/mat/charProc/ROMoffset
add wave -noupdate -expand -group {rf Read} -label {Char Row Data} /tb_LEDMatrix/mat/cROM/rData
add wave -noupdate -group Other -label tic /tb_LEDMatrix/mat/dataProc/spike
add wave -noupdate -group Other -label toc /tb_LEDMatrix/mat/dataProc/toggle
add wave -noupdate -group Output -height 30 -label {Row Select} /tb_LEDMatrix/rowDriver
add wave -noupdate -group Output -height 30 -label {Port A} /tb_LEDMatrix/portA
add wave -noupdate -group Output -height 30 -label {Port B} /tb_LEDMatrix/portB
add wave -noupdate -group Output -height 30 -label {Port C} /tb_LEDMatrix/portC
add wave -noupdate -label row -radix unsigned /tb_LEDMatrix/row
add wave -noupdate -expand -group Characters -height 30 -label {Character A} -radix binary -childformat {{{/tb_LEDMatrix/charA[6]} -radix binary} {{/tb_LEDMatrix/charA[5]} -radix binary} {{/tb_LEDMatrix/charA[4]} -radix binary} {{/tb_LEDMatrix/charA[3]} -radix binary} {{/tb_LEDMatrix/charA[2]} -radix binary} {{/tb_LEDMatrix/charA[1]} -radix binary} {{/tb_LEDMatrix/charA[0]} -radix binary}} -subitemconfig {{/tb_LEDMatrix/charA[6]} {-height 15 -radix binary} {/tb_LEDMatrix/charA[5]} {-height 15 -radix binary} {/tb_LEDMatrix/charA[4]} {-height 15 -radix binary} {/tb_LEDMatrix/charA[3]} {-height 15 -radix binary} {/tb_LEDMatrix/charA[2]} {-height 15 -radix binary} {/tb_LEDMatrix/charA[1]} {-height 15 -radix binary} {/tb_LEDMatrix/charA[0]} {-height 15 -radix binary}} /tb_LEDMatrix/charA
add wave -noupdate -expand -group Characters -height 30 -label {Character B} /tb_LEDMatrix/charB
add wave -noupdate -expand -group Characters -height 30 -label {Character C} /tb_LEDMatrix/charC
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Set Speed} {55 ps} 1} {{Write A} {146 ps} 1} {{Write B} {190 ps} 1} {{Write C} {234 ps} 1} {{Read A} {158 ps} 1} {{Read B} {214 ps} 1}
quietly wave cursor active 4
configure wave -namecolwidth 207
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {43 ps} {145 ps}
