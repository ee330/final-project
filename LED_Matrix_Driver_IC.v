module LEDMatrix(eClk, iClk, reset, sel, dBus, rowDriver, portA, portB, portC);
   parameter     DBUS_WIDTH             = 4;
   parameter     CHAR_HEIGHT            = 7;
   parameter     CHAR_WIDTH             = 5;
   parameter     CMD_WIDTH              = 8;

   parameter     S_ADDR_WIDTH           = 5;
   parameter     S_COUNT                = 1 << S_ADDR_WIDTH;
   parameter     INSTR_DATA_WIDTH       = 7;
   parameter     ASCI_WIDTH             = INSTR_DATA_WIDTH;
   parameter     ROW_ID_WIDTH           = 3;

   input                                eClk, iClk, reset, sel;
   input       [(DBUS_WIDTH-1)  :0]     dBus;
   output      [(CHAR_HEIGHT-1) :0]     rowDriver;
   output wire [(CHAR_WIDTH-1)  :0]     portA, portB, portC;
	//dataBus
   wire                                 sync;
   wire        [(CMD_WIDTH-1)   :0]     instr;
   wire                                 instr_cmd                  = instr[(CMD_WIDTH-1)];
   wire        [(ASCI_WIDTH-1)  :0]     instr_data                 = instr[(INSTR_DATA_WIDTH-1):0]; 
   //dataProcessor
   wire        [(S_ADDR_WIDTH-1):0]     rIdx, sLen;
   wire                                 setIdx;
   wire        [(INSTR_DATA_WIDTH-1):0] rfWD, speed;
   //regFile
   wire        [(S_ADDR_WIDTH-1):0]     ra, rb;
   wire                                 rfWE;
   wire        [(ASCI_WIDTH-1)  :0]     ASCI_id;
   //charProcessor
   wire                                 charWE;
   //charROM
   wire        [(ROW_ID_WIDTH-1):0]     rowIdx;
   wire        [(CHAR_WIDTH-1)  :0]     charData;
	
   //dBusReady
   //latched output only updated when full byte is read
   dataBus       #(DBUS_WIDTH, CMD_WIDTH)
            db(.clk(eClk), .reset(reset), .sel(sel), .dBus(dBus), .sync(sync), .dByte(instr));
	
   //Process Instruction from dataBus
   dataProcessor #(S_ADDR_WIDTH, INSTR_DATA_WIDTH)
             dataProc(.clk(iClk), .reset(reset), .rIdx(rIdx), .setIdx(setIdx), .sync(sync), .cmd(instr_cmd), .data(instr_data), .rfWA(rb), .rfWD(rfWD), .rfWE(rfWE), .sLen(sLen), .setSpeed(setSpeed), .speed(speed));
	
   regFile       #(S_ADDR_WIDTH, S_COUNT, ASCI_WIDTH)
             rFile(.clk(iClk), .ra(ra), .rData(ASCI_id), .rb(rb), .wData(rfWD), .WE(rfWE)); //, .RE(charWE)
		
   charROM       #(ASCI_WIDTH, ROW_ID_WIDTH, CHAR_HEIGHT, CHAR_WIDTH)
             cROM(.address(ASCI_id), .offset(rowIdx), .rData(charData));
	
	
   charProcessor #(S_ADDR_WIDTH, ROW_ID_WIDTH, INSTR_DATA_WIDTH)
             charProc(.clk(iClk), .reset(reset), .idx(rIdx), .setIdx(setIdx), .setSpeed(setSpeed), .speed(speed), .sLen(sLen), .rfRA(ra), .ROMoffset(rowIdx), .charWE(charWE));
	
   charDriver    #(CHAR_WIDTH, CHAR_HEIGHT)
             cDriverA(.clk(iClk), .reset(setIdx), .WE(charWE), .wData(charData), .rData(portA)),
             cDriverB(.clk(iClk), .reset(setIdx), .WE(charWE), .wData(portA),    .rData(portB)),
             cDriverC(.clk(iClk), .reset(setIdx), .WE(charWE), .wData(portB),    .rData(portC));

   encoder       #(ROW_ID_WIDTH, CHAR_HEIGHT)
             rEncoder(.clk(iClk), .dIn(rowIdx), .dOut(rowDriver));
	
endmodule
