module encoder(clk, dIn, dOut);
   parameter    INPUT_WIDTH;
   parameter    OUTPUT_WIDTH;
	
   input                           clk;
   input      [(INPUT_WIDTH-1) :0] dIn;
   output     [(OUTPUT_WIDTH-1):0] dOut;
   assign dOut = 1'b1 << dIn;
endmodule
